function add(a,b){
    return a + b;
}
function reduce(a,b){
    return a - b;
}
function multiple(a,b){
    return a * b;
}
function divide(a,b){
    return a / b;
}
//暴露
module.exports = {
    add:add,
    reduce:reduce,
    multiple:multiple,
    divide:divide
}